import xml.sax
import classifier
import traininghelper
import re
import mwparserfromhell
import matplotlib.pyplot as plt
from svc import SMO
import sys
import math
import pysolr
sys.setrecursionlimit(100000)

import bz2

trainingSet = []
trainingLabels = []
astro_data = traininghelper.TrainingDataHelper.get_astro_training_data()
non_astro_data = traininghelper.TrainingDataHelper.get_non_astro_training_data()
training_classes = []
for article in astro_data:
    trainingSet.append(article.text)
    trainingLabels.append("Astronomy")
    training_classes.append(1)
for article in non_astro_data[1:125]:
    trainingSet.append(article.text)
    trainingLabels.append("Non Astronomy")
    training_classes.append(-1)

#cls = SMO(3,0.001,pow(2,-5))
#data = cls.get_vectors(trainingSet)
#cls.train(data,training_classes)
cls = classifier.Classifier(1000)
cls.train(trainingSet,trainingLabels)
file = bz2.open("/tadata/users/sridhar/enwiki-latest-pages-articles.xml.bz2","r");

#
# fp = 0
# fn=0
# tp=0
# tn = 0
# count = 0
# a_x = []
# a_y=[]
# na_x=[]
# na_y=[]
# for article in astro_data:
#      res = cls.classify(article)
#   #   probs = cls.getprobabilities(article)[0]
#      if res == 'Non Astronomy':
#       fn=fn+1
#      else:
#       tp = tp + 1
# #     # print(article.title,res)
# #     a_x.append(probs[0])
# #     a_y.append(probs[1])
# for article in non_astro_data:
#      res = cls.classify(article)
# #     probs = cls.getprobabilities(article)[0]
#      if res == 'Astronomy':
#         fp = fp+1
#      else:
#       tn=tn+1
# #     # print(article.title,res);
# #     na_x.append(probs[0])
# #     na_y.append(probs[1])
#
#
#
# precision = (tp)/(tp+fp)
# recall = (tp)/(tp+fn)

def find_all(a_string, sub):
    result = []
    k = 0
    while k < len(a_string):
        k = a_string.find(sub, k)
        if k == -1:
            return result
        else:
            result.append(k)
            k += 1 #change to k += len(sub) to not search overlapping results
    return result



class WikiHandler(xml.sax.ContentHandler):

    def __init__(self):
        """
        Initialize variables required for parsing Wikipedia XML, classifying articles and adding it to SOLR
        """
        self._charBuffer = []
        self.count = 0
        self._result = []
        self._page=False
        self._txt=False
        self._timestamp=False
        self._id=False
        self.Id = None
        self.Timestamp = ""
        self.Title = ""
        self.Text = ""
        self._title = False
        self.astroCount = 0
        self._solrClient = pysolr.Solr("http://cityhunter.as.arizona.edu:8983/solr/TA_Wikipedia",timeout=20)

        self.results = open("/tadata/users/sridhar/SVM-Astro-Solr.txt", 'w', errors='ignore')


    def _getCharacterData(self):
        data = ''.join(self._charBuffer).strip()
        self._charBuffer = []
        return data # remove strip() if whitespace is important

    def parse(self, f):
        xml.sax.parse(f, self)
        return self._result

    def characters(self, data):

        if self._id == True:
            self.Id = data
            self._id = False
        if self._txt == True:
            self._charBuffer.append(data)
        if self._title == True:
            self.Title = data
            self._title = False
        if self._timestamp == True:
            self.Timestamp = data
            self._timestamp = False

    def startElement(self, name, attrs):
        if name == 'page': self._page=True;
        if name == 'text':
            self._txt = True
            self._charBuffer=[]
        if name == 'timestamp': self._timestamp = True;
        if name == 'id':
            if self.Id is None or self.Id == "":
                self._id = True
        if name == 'title': self._title = True;

    def checkInfoBox(self,title,text):
        """
        Check if given article text or title matches Infobox patterns specific to astronomy
        :param title: title of article
        :param text: text of article
        :return: 1 or -1 indicating whether article is an Astronomy article based on Infobox patterns verified.
        """
        if(title.startswith("file:") or title.startswith("wikipedia:") or title.startswith("portal:") or title.startswith("template:")):
            return -1

        if("Infobox" in text):
            if ("(\\[\\[Category:).*?(astronomers\\]\\])" in text or
                    "(\\[\\[Category:).*?(Astronomers\\]\\])" in text or
                    "(\\[\\[Category:).*?(NASA\\]\\])" in text or
                    "(\\[\\[Category:).*?(Space program\\]\\])" in text):
                return 1
            elif ("Infobox planet" in text  or "Infobox astronomical survey" in text  or "Infobox comet" in text
                    or "Infobox constellation" in text
                    or "Infobox nebula" in text
                    or "Infobox galaxy" in text
                    or "Infobox star" in text
                    or "Infobox launch pad" in text
                    or "Infobox rocket" in text
                    or "Infobox rocket engine" in text
                    or "Infobox rocket stage" in text
                    or "Infobox space program" in text
                    or "Infobox space shuttle" in text
                    or "Infobox space station" in text
                    or "Infobox space station module" in text
                    or "Infobox spacecraft" in text
                    or "Infobox spacecraft class" in text
                    or "Infobox spacecraft instrument" in text
                    or "Infobox spaceflight" in text
                    or "Infobox year in spaceflight" in text
                    or "Infobox cluster" in text
                    or "Infobox crater data" in text
                    or "Infobox feature on celestial object" in text
                    or "Infobox galaxy cluster" in text
                    or "Infobox GRB" in text
                    or "Infobox globular cluster" in text
                    or "Infobox lunar crater or mare" in text
                    or "Infobox magnetosphere" in text
                    or "Infobox meteor shower" in text
                    or "Infobox meteorite" in text
                    or "Infobox meteorite subdivision" in text
                    or "Infobox navigation satellite system" in text
                    or "Infobox quasar" in text
                    or "Infobox solar cycle" in text
                    or "Infobox solar eclipse" in text
                    or "Infobox supercluster" in text
                    or "Infobox supernova" in text):
                        return 1

        return 0

    def endElement(self, name):
        if name == 'text':
            self.Text = self._getCharacterData()
            self._txt = False
        if name == 'page':
            wikiarticle = traininghelper.WikiArticle()
            wikiarticle.id = self.Id
            self.Id = None
            rawText = self.Text
            text = self.Text.lower()
            rawTitle = self.Title
            plaintext = re.sub("[^ a-zA-Z]", ' ', text)
            plaintext = re.sub("\\s+", ' ', plaintext)
            wikiarticle.text = plaintext
            wikiarticle.timestamp = self.Timestamp
            full_title = self.Title
            wikiarticle.title = self.Title.lower()
            self.count += 1
           # prob = cls.getprobabilities(wikiarticle);
            isInfobox = self.checkInfoBox(rawTitle,rawText)
            if isInfobox == 1:
                cats = find_all(self.Text, '[[Category:')
                categories = []
                for cat in cats:
                    temp = ""
                    j = cat
                    while self.Text[j + 11] != ']':
                        temp = temp + self.Text[j + 11]
                        j += 1
                    categories.append(temp)
                doc = [{"articleId":wikiarticle.id,"articleTitle":full_title,"articleText":wikiarticle.text,"categories":categories}]
                self._solrClient.add(doc)
                print(rawTitle + " - Infobox")
            else:

                if text.startswith("#redirect")==False and wikiarticle.title.startswith("category:")==False and wikiarticle.title.startswith("wikipedia:")==False:
                    result = cls.classify(wikiarticle)


                    if result == 'Astronomy':
                        self.astroCount+=1
                        cats = find_all(self.Text,'[[Category:')
                        categories = []
                        for cat in cats:
                            temp = ""
                            j = cat
                            while self.Text[j+11]!=']':
                                temp = temp+self.Text[j+11]
                                j+=1
                            categories.append(temp)
                        doc = [{"articleId":wikiarticle.id,"articleTitle":full_title,"articleText":wikiarticle.text,"categories":categories}]
                        self._solrClient.add(doc)
                        probs = cls.getprobabilities(wikiarticle)[0]
                        a_prob = str(math.log(probs[0],10))
                        na_prob = str(math.log(probs[1],10))
                        rslt = wikiarticle.title+","+a_prob+","+na_prob
                        print(rslt)
                        self.results.write(rslt)
                        self.results.write("\n")
                    #self.results.flush()





xml.sax.parse(file, WikiHandler())



