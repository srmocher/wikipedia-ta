from os import listdir
from os.path import isfile, join
import re
import mwparserfromhell
from nltk.stem import PorterStemmer
from sklearn.feature_extraction.text import CountVectorizer

class WikiArticle:
    def __init__(self):
        self.title = ""
        self.text = ""
        self.id = ""
        self.timestamp = ""
        self.label = ""


class TrainingDataHelper:
    @staticmethod
    def get_astro_training_data():
        """
        Get list of articles labelled "Astronomy" from training set
        :return: List of WikiArticle objects with labels
        """
        files = [f for f in listdir("/tadata/users/sridhar/TrainingData/Astronomy") if isfile(join("/tadata/users/sridhar/TrainingData/Astronomy", f))]
        articles = []
        for file in files:
            title = file
            print(title)
            fle = open("/tadata/users/sridhar/TrainingData/Astronomy/"+file,encoding="utf-8",errors="ignore")
            text = fle.read()


            title = title.lower()
            text = re.sub("[^ a-zA-Z]", ' ', text)
            text = re.sub("\\s+", ' ',text)
            article = WikiArticle()
            article.text = text.lower()
            article.title = title
            article.label = "Astronomy"
            articles.append(article)
        return articles

    @staticmethod
    def get_non_astro_training_data():
        """
        Return a list of articles labeled "Non Astronomy" from training set
        :return:
        """
        files = [f for f in listdir("/tadata/users/sridhar/TrainingData/Non Astronomy") if isfile(join("/tadata/users/sridhar/TrainingData/Non Astronomy", f))]
        articles = []
        i=0
        for file in files:
            title = file.split(".")[0]
            fle = open("/tadata/users/sridhar/TrainingData/Non Astronomy/" + file, encoding="utf-8")
            text = fle.read()

            plaintext = re.sub("[^ a-zA-Z]", ' ', text)
            plaintext = re.sub("\\s+", ' ', text)
            article = WikiArticle()
            article.text = text.lower()
            article.title = title
            article.label = "Non Astronomy"
            articles.append(article)

        return articles

    @staticmethod
    def get_stop_words():
        words = []
        for line in open("/tadata/users/sridhar/stopWords.txt", 'r',errors="ignore").read().splitlines():
            words.append(line)
        for line in open("/tadata/users/sridhar/wikipediaStopWords.txt", 'r',errors="ignore").read().splitlines():
            words.append(line)
        return words


