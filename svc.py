import numpy as np
import random
from sklearn.feature_extraction.text import *
from nltk.stem import PorterStemmer
from traininghelper import TrainingDataHelper


class SMO:
    def __init__(self, m_passes, tol, C):

        self.bias = 0
        self.max_passes = m_passes
        self.tol = tol
        self.C = C

    def findBounds(self, yi ,yj ,C , alphai, alphaj):
        if yi == yj:
            L = max(0, alphai + alphaj - C)
            H = min(C, alphai + alphaj)
        else:
            L = max(0, alphaj - alphai)
            H = min(C, C + alphai - alphaj)
        return L, H

    def train(self,training_vectors,training_labels):
        passes = 0

        m = len(training_labels)
        self.alphas = [0] * m
        for i in range(1, m):
            self.alphas[i] = 0
        x = training_vectors
        y = training_labels
        self.x = x
        self.y = y
        while passes < self.max_passes:
            num_changed_alphas = 0
            for i in range(0, m-1):
                err1 = self.f(x[i], x, y, self.alphas, self.bias) - y[i]
                if ((err1*y[i] < -self.tol) and (self.alphas[i] < self.C)) or (err1*y[i] > self.tol and self.alphas[i] > 0):
                    j = random.randint(0, m-1)
                    if j != i:
                        err2 = self.f(x[j], x, y, self.alphas, self.bias) - y[j]
                        alpha1_old = self.alphas[i]
                        alpha2_old = self.alphas[j]
                        L, H = self.findBounds(y[i], y[j], self.C, alpha1_old, alpha2_old)
                        if L == H:
                            continue
                        n = 2*self.kernel(x[i], x[j]) - self.kernel(x[i], x[i])-self.kernel(x[j], x[j])
                        if n >= 0:
                            continue
                        self.alphas[j] -= (y[j]*(err1 - err2))/n
                        self.alphas[j] = self.clip(self.alphas[j], L, H)
                        if abs(alpha2_old - self.alphas[j]) < pow(10, -5):
                            continue
                        self.alphas[i] += y[i]*y[j]*(alpha2_old - self.alphas[j])
                        b1 = self.bias - err1 - y[i]*(self.alphas[i]-alpha1_old)*self.kernel(x[i], x[i])-y[j]*(self.alphas[j]-alpha2_old)*self.kernel(x[i], x[j])
                        b2 = self.bias - err2 - y[i]*(self.alphas[i]-alpha1_old)*self.kernel(x[i], x[j])-y[j]*(self.alphas[j]-alpha2_old)*self.kernel(x[j], x[j])
                        self.bias = self.compute_bias(b1, b2, self.alphas[i], self.alphas[j], self.C)
                        num_changed_alphas += 1
            if num_changed_alphas == 0:
                passes += 1
            else:
                passes = 0


    def predict(self,text):
        y = self.vectorizer.transform([text]).toarray()[0]
        return self.f(y,self.x,self.y,self.alphas,self.bias)

    def f(self,p,x,y,alphas,b):
        m = len(x)
        res = 0
        for i in range(0,m-1):
            k = self.kernel(x[i],p)
            res += alphas[i]*y[i]*k
        return res + b


    def kernel(self, x1, x2):

        prod = np.dot(x1, x2)
        return prod

    def clip(self,alpha,L,H):
        if alpha > H:
            return H
        elif alpha < L:
            return L
        else:
            return alpha

    def compute_bias(self,b1,b2,alphai,alphaj,C):
        if alphai > 0 and alphai < C:
            return b1
        elif alphaj > 0 and alphaj < C:
            return b2
        else:
            return (b1+b2)/2

    def get_vectors(self,training_data):
        self.stemmer = PorterStemmer()
        stopWords = TrainingDataHelper.get_stop_words()
        self.analyzer = CountVectorizer().build_analyzer()

        def stemmed_words(doc):
            return (self.stemmer.stem(w) for w in self.analyzer(doc))

        # for doc in trainingData:
        #     words = doc.split(" ")
        #     for word in words:
        #         stemmedWord = self.stemmer.stem(word)
        #         if word not in vocabulary:
        #             vocabulary.add(word)
        self.vectorizer =CountVectorizer(stop_words=stopWords, analyzer=stemmed_words)

       # vectorizer.fit(training_data)
        data = self.vectorizer.fit_transform(training_data)
        return data.toarray()





