from sklearn.feature_extraction.text import *
from sklearn.svm import *
from traininghelper import TrainingDataHelper
from nltk.stem import *
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import *
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import *
from sklearn.pipeline import *
from sklearn.model_selection import KFold


class Classifier:
    def __init__(self,c):
        """
        Initialize SVM classifier with linear kernel using SVC class
        :param c: regularization parameter
        """
        self.classifier = SVC(random_state=0,C=c,kernel='linear',probability=True)

    def train(self,trainingData,trainingLabels):
        """
        Method to train Wikipedia articles using SVC initialized in init()
        :param trainingData: List of article texts
        :param trainingLabels: List of labels(1 or -1) identifying corresponding training articles
        :return:
        """
        vocabulary= set()
        self.stemmer = PorterStemmer()
        stopWords = TrainingDataHelper.get_stop_words()
        analyzer = TfidfVectorizer().build_analyzer()

        def stemmed_words(doc):
            return (self.stemmer.stem(w) for w in analyzer(doc))
        # for doc in trainingData:
        #     words = doc.split(" ")
        #     for word in words:
        #         stemmedWord = self.stemmer.stem(word)
        #         if word not in vocabulary:
        #             vocabulary.add(word)
        self.vectorizer = TfidfVectorizer(stop_words=stopWords,analyzer=stemmed_words,sublinear_tf=True)



        self.vectorizer.fit(trainingData)
        data = self.vectorizer.transform(trainingData)
        print(self.vectorizer.vocabulary_)
        self.classifier.fit(data,trainingLabels)

    def classify(self,wikiArticle):
        """
        Classifying a new article using trained SVC
        :param wikiArticle: new article text
        :return: label - "Astronomy" or "Non Astronomy"
        """
        try:

            data = self.vectorizer.transform([wikiArticle.text])
            if wikiArticle.text.startswith("#redirect"):
                return "Non Astronomy"
            #   print(data);
            result = self.classifier.predict(data)

            return result
        except ValueError:
            print("error")

    def getprobabilities(self,wikiarticle):
        """
        Return probabilites for both classes for the given article classified using SVC
        :param wikiarticle: article text
        :return: list of two probabilities for each class
        """
        data = self.vectorizer.transform([wikiarticle.text])
        return self.classifier.predict_proba(data)



